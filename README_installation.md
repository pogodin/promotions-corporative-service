# Installation of the frameworks and tools for the Promotion Corporate Service #

Promotion Corporate Service is based on several frameworks.
Most of them needs to be installed before customization and usage.
This guide will follow through the installation process.
!! Requires Ubuntu OS

### Table of contents ###

* Nifi
* Kafka
* MongoDB
* Spark
* Redis
* NodeJS

### Nifi ###

* install JRE
    `sudo apt-get install default-jre`

* download and unpack
    ```
    wget http://apache.cs.utah.edu/nifi/0.6.1/nifi-0.6.1-bin.tar.gz
    tar xvf nifi-0.6.1-bin.tar.gz
    sudo mv nifi-0.6.1 /etc/nifi
    cd /etc/nifi/
    ```
* add memory in the config
    `vim conf/bootstrap.conf`
    `java.arg.2=-Xms2g`
    `java.arg.3=-Xms2g`
    `java.arg.11=-XX:PermSize=2g`
    `java.arg.12=-XX:MaxPermSize=2g`
    
* install as a service
    `bin/nifi.sh install`

### Kafka ###

* install
    ```
    sudo apt-get install default-jre
    sudo apt-get install zookeeperd
    wget http://mirror.reverse.net/pub/apache/kafka/0.10.0.0/kafka_2.11-0.10.0.0.tgz
    tar -xvzf kafka_2.11-0.10.0.0.tgz
    sudo mv kafka_2.11-0.10.0.0 /etc/kafka
    cd /etc/kafka/
    ```
* set up
    `vim config/server.properties`
    `shift` + `g`
    `o`
    paste the following line `delete.topic.enable = true`
    save `shift` + `z` + `z` or `:wq`
* start
    `nohup /etc/kafka/bin/kafka-server-start.sh /etc/kafka/config/server.properties > /etc/kafka/kafka.log 2>&1 &`
* test
    `echo "Hello, World" | /etc/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic TutorialTopic > /dev/null`
    `/etc/kafka/bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic TutorialTopic --from-beginning`
    should see `Hello, World`


### MongoDB ###

* `sudo apt-get install -y mongodb`
* also [this](http://askubuntu.com/questions/724749/install-mongo-3-2-on-ubuntu-15-10) could be in use
* default port is 27017

### Spark ###

* download spark. Find mirror on [download page](http://www.apache.org/dyn/closer.lua/spark/spark-1.6.2/spark-1.6.2-bin-hadoop2.6.tgz)
* `tar zxvf spark-1.6.2-bin-hadoop2.6.tgz`

### Redis ###

* Follow the [instruction](https://scaleyourcode.com/blog/article/3)
    * except version is `3.2.1`
    * default port is 6379
* to set up Redis Cluster: https://ilyabylich.svbtle.com/redis-cluster-quick-overview
    * in case of exception: ERR Slot 0 is already busy do `sudo rm -rf /var/lib/redis/6379/*`  

### NodeJS ###

https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server
node-redis-rest
===============

Simple Node.js application with Redis and REST API.
Forked from https://github.com/nickholub/node-redis-rest

## Usage

 Clone the repo and install dependencies:

    $ npm install

 Run Redis server: (port and host defined in config)

    $ <redis>/src/redis-server

 Start Node.js server:

    $ node app

  Test REST API, e.g.

    for single field request\response :
    http://localhost:3000/single/1/2/3/4/5

    for multiply fields request\response:
    http://localhost:3000/multi?keys=1,2,3,4,5&keys=2,3,4,5,6&keys=3,4,5,6,7... etc.

    IF field does not exists -> empty object response
    IF was offered less than 5 identifiers
        (E.G. http://localhost:3000/multi?keys=1,2,3,4) -> Exception


  response should looks like :

      {
        "field1": "hash1",
        "field2": "hash2",
        "$set_prefix": [
          "{hello:{omg:json}, again:omg},
          "{omg:json, again:omg}"
        ]
      }

## Load testing :

1. execute command : npm install -g artillery
2. go to loadtest folder
3.
 a. UBUNTU option :
    execute command : ./full_report_run.sh -> this command will create additional folder. Run it directly from folder where full_report_run.sh located
 b. NOT BASH-SCRIPT OPTION
    execute command : artillery run loadtest.json
    execute command : artillery report artillery_report_???????_??????.json
        Where ??????_????? are numbers that represents current timestamp.
    open file : artillery_report_???????_??????.json.html in browser

## Dependencies

[node_redis](https://github.com/mranney/node_redis) Node.js Redis client

[Express](https://github.com/visionmedia/express) Node.js web framework

#!/usr/bin/env bash
mkdir -p log;
echo "moving old reports to log directory"
find ./ -maxdepth 0 -name 'artillery_report*' -exec mv {} log/ \;
echo "starting loadtest loadtest.json"
artillery run loadtest.json;
fn=$(ls -t| grep "artillery_report.*.json$" | head -n1);
echo "generating html report according to test"
artillery report $fn;
#echo "opening html report in browser"
#sensible-browser "$fn.html";


var express = require('express');
var redis = require("redis");
var redisScanner = require("redis-scanner");
var config = require('./config');
var stringTemplateCompile = require("string-template/compile");

var app = express();
var redisClient = redis.createClient(config.redis.port, config.redis.host);
redisScanner.bindScanners(redisClient);
redisClient.on("error", function (err) {
    console.log("Redis Error: " + err);
});

app.get('/single/:p1/:p2/:p3/:p4/:p5', getSingleRestKeySet);
app.get('/single/:p1/:p2/:p3/:p4', getSingleRestKeySet);

function getSingleRestKeySet(req, res) {
    var params = [req.params.p1, req.params.p2, req.params.p3, req.params.p4, req.params.p5 || ''];
    // console.log('received single request for : ' + params);
    if (params.length !== 5 && params.length !== 4) {
        // should never happen
        throw "Illegal amount of parameters, should be exactly 4 or 5, but actual params: [" + params + "]";
    }

    var refRedisKey = "r:" + params.join(":");
    // console.log('searching for reference: ' + refRedisKey);

    redisClient.get(refRedisKey, function(err, reply) {
        if (err) {
            console.error("error searching reference: " + err);
            return res.send("Error: " + err, 500)
        }
        if (reply) {
            // console.log('found reference: ' + reply);
            var promoRedisKeySuffixParts = reply.split(" ");
            if (promoRedisKeySuffixParts.length != 3) {
                console.error("reference redis value has params with length different than 3: " + promoRedisKeySuffixParts)
                return res.send("Error: redis has wrong reference entry", 500)
            }

            var props = params.concat(promoRedisKeySuffixParts);
            // console.log("searching for promos: " + props);
            getPromos(props, function (err, promosAsJsonArray) {
                if (err) {
                    console.error("error searching promos: " + err);
                    return res.send("Error extracting promotions: " + err, 500);
                }

                return res.json({
                    "CEMPRESA": props[0],
                    "CDEPARTM": props[1],
                    "CFAMILIA": props[2],
                    "CBARRAAA": props[3],
                    "CTALLAEC": props[4],
                    "CDIVISIO": props[5],
                    "CFABRICA": props[6],
                    "CMARMUMA": props[7],
                    "promotionsNum": promosAsJsonArray.length,
                    "promotions": promosAsJsonArray.slice(0, 3)
                })
            });
            return;
        }

        return res.send("NotFound", 404);
    });
}

var promoKeysTemplate = stringTemplateCompile(config.redis.promokeytemplates, true);

function getPromos(props, callback) {
    if (!props || props.length != 8) {
        console.error("params array incorrect size detected " + props.size);
        return;
    }
    redisClient.send_command("SUNION", promoKeysTemplate(props).split(" "),
        function (err, data) {
            if (err) {
                console.error("error while loading promos for ref props " + props + ": " + err);
                return callback("db error: " + err);
            }
            // console.log('found ' + data.length + " promos for ref props " + props);
            // callback(null, data.length);
            callback(null, data);
        });
}

app.listen(config.port);
if (config.port) {
    console.log('Express started on port ' + config.port);
} else {
    console.log('Can\'t start express. config.port is ' + config.port);
}

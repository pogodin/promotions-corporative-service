var config = {};

config.port = process.env.PORT || 3000;

config.redis = {};
config.redis.host = process.env.REDIS_HOST || "127.0.0.1";
config.redis.port = process.env.REDIS_PORT || 6379;

// !! uncomment only one of the following two:
// doesn't contain "all refs promos"
config.redis.promokeytemplates = "p:{0}:{1}:{2}:{3}:{4}:-:-:- p:{0}:{1}:{2}:{3}:-:-:-:- p:{0}:{1}:{2}:-:-:-:-:{7} p:{0}:{1}:-:-:-:-:-:{7} p:{0}:{1}:{2}:-:-:-:{6}:- p:{0}:{1}:-:-:-:-:{6}:- p:{0}:{1}:{2}:-:-:-:-:- p:{0}:{1}:-:-:-:-:-:- p:{0}:-:-:-:-:{5}:-:- p:{0}:-:-:-:-:-:-:-"
// contains "all refs promos"
// config.redis.promokeytemplates = "p:{0}:{1}:{2}:{3}:{4}:-:-:- p:{0}:{1}:{2}:{3}:-:-:-:- p:{0}:{1}:{2}:-:-:-:-:{7} p:{0}:{1}:-:-:-:-:-:{7} p:{0}:{1}:{2}:-:-:-:{6}:- p:{0}:{1}:-:-:-:-:{6}:- p:{0}:{1}:{2}:-:-:-:-:- p:{0}:{1}:-:-:-:-:-:- p:{0}:-:-:-:-:{5}:-:- p:-:-:-:-:-:-:-:- p:{0}:-:-:-:-:-:-:-"

module.exports = config;

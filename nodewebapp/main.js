var cluster = require('cluster');
var args = process.argv.slice(2);

if (args.length !== 1) {
    console.log("number of threads is not specified. No cluster.")
    require('./app');
    return;
}

var numCPUs = parseInt(args[0], 10);

if (cluster.isMaster) {
    console.log("Master is creating " + numCPUs + " threads")
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
} else {
    console.log("creating a worker");
    require('./app');
}
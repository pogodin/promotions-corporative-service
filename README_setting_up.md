# Promotions Corporate Service setting up #

This guide describes how to set up Promotions Corporate Service moving
parts so that it will be ready for usage: references and
promotions importing, then requesting promotions by references

This guide assumes that the all the frameworks are installed according to [installation README](README_installation.md)

### Nifi modules ###

The Nifi application has 4 modules:

* references migration from file to MongoDB
* promotions migration from file to MongoDB
* references migration from MongoDB to Kafka
* promotions migration from MongoDB to Kafka

##### Setting up Nifi #####

* assumes that Nifi is already installed according to [installation README](README_installation.md)
* create folder structure on the host containing Nifi assuming the user name is ubuntu
    ```
    sudo mkdir /nififiles
    sudo chown ubuntu /nififiles
    mkdir -p /nififiles/refs/in /nififiles/refs/fail/parse /nififiles/refs/fail/parse2 /nififiles/refs/fail/mongo /nififiles/refs/fail/kafka
    mkdir -p /nififiles/promos/in /nififiles/promos/fail/parse /nififiles/promos/fail/parse2 /nififiles/promos/fail/mongo /nififiles/promos/fail/kafka
    ```
* upload template
    * click at top right corner blue button "Templates"
    * upload template from folder nifi-templates of this application
* use the template to create nifi processors
    * drag-and-drop template - bigger blue icon on the instruments panel at the top
    * chose the template you uploaded
* specify MongoDB and Kafka addresses
    * in refs-files-to-mongo
        * right click "Put to MongoDB"
        * Configure
        * Properties tab
        * change Mongo URI and Mongo database name
    * in promos-files-to-mongo
        * right click "Put to MongoDB"
        * Configure
        * Properties tab
        * change Mongo URI and Mongo database name
    * in refs-mongo-to-kafka
        * right click "Put to Kafka"
        * Configure
        * Properties tab
        * change Known Brokers specifying Kafka broker(s) url(s)
    * in promos-mongo-to-kafka
        * right click "Put to Kafka"
        * Configure
        * Properties tab
        * change Known Brokers specifying Kafka broker(s) url(s)

### Spark application ###

* assumes that Spark is already installed according to [installation README](README_installation.md)
* add/change ~/spark/spark-1.6.2-bin-hadoop2.6/conf/log4j.properties
    * to be like the file from the current source: `/spark-streaming/log4j.properties` 
* get application binary to the host with spark streaming installed
    * build the application or take its pre-built version in `spark-streaming/promotions-service-spark-streaming-assembly-1.0.jar`
    * upload to the spark host
* run the application:
    `~/spark/spark-1.6.2-bin-hadoop2.6/bin/spark-submit --class manawa.promoservice.Main --master local[8] ~/promotions-service-spark-streaming-assembly-1.0.jar \<kafkaBrokersUrl\>:9092 \<mongoHostUrl\> \<redisHostUrl\>`
    for instance: `~/spark/spark-1.6.2-bin-hadoop2.6/bin/spark-submit --class manawa.promoservice.Main --master local[8] ~/promotions-service-spark-streaming-assembly-1.0.jar localhost:9092 localhost localhost`
    or `nohup ~/spark/spark-1.6.2-bin-hadoop2.6/bin/spark-submit --class manawa.promoservice.Main --master local[8] ~/promotions-service-spark-streaming-assembly-1.0.jar localhost:9092 localhost 172.31.19.49 > ~/spark/spark.log 2>&1 &`

### Node.js application ###

* assumes that NodeJS is already installed according to [installation README](README_installation.md)
* specify in config.js the redis host and port
* upload the application from the `nodewepapp` directory of this source to the host with pre-installed NodeJS
    * there are only a few files needed: app.js, config.js, main.js, package.json  
* build the application at the host `npm install`
* start the application at the host as a cluster `nohup node main.js 16 > nodewebapp.log 2>&1 &`
    * 16 means the number of instances in cluster   

### Kafka instance ###

* assumes that Kafka is already installed according to [installation README](README_installation.md)
* create topics:
    * `/etc/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --create --topic promos --partitions 20 --replication-factor 1`
    * `/etc/kafka/bin/kafka-topics.sh --zookeeper localhost:2181 --create --topic refs --partitions 20 --replication-factor 1`
* couple of tips:
    * topics are also created once Nifi starts the importing to Kafka
    * if topics are not created, the Spark application will fail to start

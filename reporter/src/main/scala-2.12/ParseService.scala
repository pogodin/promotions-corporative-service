trait ParseService {

  private final val totalSize = 209

  def mkReport(r: Refs, p: Promo): String = {

    import ParseService.StringTrimmerUltra
    val builder = StringBuilder.newBuilder

    .append(r.cempresa.trim(Cempresa.size))
    .append("    ".trim(Centrooo.size))
    .append("  ".trim(Canalvta.size))
    .append(p.cpromoci.trim(Cpromoci.size))
    .append(r.cdepartm.trim(Cdepartm.size))
    .append(r.cfamilia.trim(Cfamilia.size))
    .append(r.cbarraaa.trim(Cbarraaa.size))
    .append(r.ctallaec.trim(Ctallaec.size))
    .append("  ".trim(Dtipprom.size))
    .append(r.cdivisio.trim(Cdivisio.size))
    .append(" ".trim(Cnivelin.size))
    .append(" ".trim(Xexcluye.size))
    .append(r.cfabrica.trim(Cfabrica.size))
    .append(r.cmarmuma.trim(Cmarmuma.size))
    .append(p.finiefec.trim(Finiefec.size))
    .append(p.ffinefec.trim(Ffinefec.size))
    .append(p.choraini.trim(Choraini.size))
    .append(p.chorafin.trim(Chorafin.size))
    .append("   ".trim(Cemprvta.size))
    .append("    ".trim(Ccentvta.size))
    .append((" "* 20).trim(Despromo.size))
    .append((" " * 13).trim(Ccarpeta.size))
    .append((" " * 50).trim(Descarpe.size))
    .append((" " * 3).trim(Coorigen.size))
    .append((" " * 24).trim(Codplaex.size))
    .append((" " * 4).trim(Chordiad.size))
    .append((" " * 4).trim(Chordiah.size))

    if (builder.size != totalSize) {

      println(s"total size  != $totalSize and equals : ${builder.size}")
    }
    builder.toString()
  }

}

object ParseService {

  implicit class StringTrimmerUltra(s: String) {
    def trim(size: Int): String = {
      if (s.length > size) {
        //println(s"trimming: '$s' to size $size")
        s.substring(0, size)
      } else if (s.length < size) {
        //println(s"trimming: '$s' to size $size")
        (" " * (size - s.length)) + s
      } else s
    }
  }

}

/**
  *
  *
  */


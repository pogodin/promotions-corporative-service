import com.redis.RedisClient

import scala.collection.immutable.Seq

trait RedisDao {

  val redisHost: String = "localhost"
  val redisPort: Int = 6379


  private val dbRPrefix = "r"
  private lazy val redisClient: RedisClient = new RedisClient(redisHost, redisPort)

  /**
    *
    * @param cursor current cursor;
    * @param count  number of elements to scan;
    * @return (cursor, seq(references));
    *         when cursor = None => foreach ended
    */
  def getAllRefs(cursor: Int = 0, cycle: Boolean = false, count: Int = 1000): (Option[Int], Seq[Refs]) = {
    val scanRes = redisClient.scan(cursor, s"$dbRPrefix:*", count)
    //println(scanRes)
    val cursorLocal = scanRes.get._1.get
    val cursorOut = if (cursorLocal == 0) None else Some(cursorLocal)
    val stringSeq: Seq[String] = scanRes.get._2.get.flatten

    val resSeq = stringSeq.map(s => {
      val key = s.split("\\:").drop(1)
      val value = redisClient.get(s).get.split(" ", 3)
      Refs(key(0), key(1), key(2), key(3), if (key.length > 4) key(4) else "", value(0), value(1), value(2))
    })

    if(cycle) {
      resSeq ++ {
        if (cursorOut.isEmpty) Seq()
        else getAllRefs(cursorOut.get)._2
      }
    }

    (cursorOut, resSeq)
  }

  def getPromo(ref: Refs): Seq[Promo] = {
    val patterns = stringPatterns(ref)
    val jsonSet: Set[String] = redisClient.sunion(patterns.head, patterns.tail).get.flatten
    jsonSet.map(s => {
      val res: Array[String] = s.replaceAll("\"", "").split("(,|\\:|\\}|\\{)").drop(1)
      Promo.fromRedisResult(res)
    }).toList
  }

  /**
    * p:p1:p2:p3:p4:p5:-:-:-
    * p:p1:p2:p3:p4:-:-:-:-
    * p:p1:p2:p3:-:-:-:-:p8
    * p:p1:p2:-:-:-:-:-:p8
    * p:p1:p2:p3:-:-:-:p7:-
    * p:p1:p2:-:-:-:-:p7:-
    * p:p1:p2:p3:-:-:-:-:-
    * p:p1:p2:-:-:-:-:-:-
    * p:p1:-:-:-:-:p6:-:-
    * p:-:-:-:-:-:-:-:-
    * p:p1:-:-:-:-:-:-:-
    * @param r
    *
    *          "r:1:4:136:16301:45" 5 306217 CALVINKLEIN
    *          p:1:4:136:16301:45
    *
    */
  private def stringPatterns(r: Refs) = {
    val p = Refs.unapply(r).get
    Seq(
      s"p:${p._1}:${p._2}:${p._3}:${p._4}:${p._5}:-:-:-",
      s"p:${p._1}:${p._2}:${p._3}:${p._4}:-:-:-:-",
      s"p:${p._1}:${p._2}:${p._3}:-:-:-:-:${p._8}",
      s"p:${p._1}:${p._2}:-:-:-:-:-:${p._8}",
      s"p:${p._1}:${p._2}:${p._3}:-:-:-:${p._7}:-",
      s"p:${p._1}:${p._2}:-:-:-:-:${p._7}:-",
      s"p:${p._1}:${p._2}:${p._3}:-:-:-:-:-",
      s"p:${p._1}:${p._2}:-:-:-:-:-:-",
      s"p:${p._1}:-:-:-:-:${p._6}:-:-",
      s"p:${p._1}:-:-:-:-:-:-:-")
  }
}



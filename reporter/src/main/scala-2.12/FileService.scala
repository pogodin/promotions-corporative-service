import java.io.{File, FileWriter}
import java.text.SimpleDateFormat
import java.util.Date

trait FileService {
  private val format = new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss")
  private val maxLinesInFile = 100000

  private var linesInFile = 0
  private var fw: FileWriter = _

  def writeToFile(data: String, path: String): Unit = {
    if (fw == null) {
      fw.recreate(path)
    }
    fw.write(data)
    linesInFile += 1
    if (linesInFile > maxLinesInFile) {
      fw.recreate(path)
      linesInFile = 0
    }
  }

  implicit class FileWriterHelper(fwOld: FileWriter) {
    def recreate(path: String) {
      if (fwOld != null) {
        fwOld.close()
      }
      val dir = new File(path)
      if (!dir.exists()) {
        dir.mkdirs()
      }
      val reportPath = path + "/report_" + format.format(new Date())
      println(s"created $reportPath")
      fw = new FileWriter(reportPath, true)
    }
  }

  def closeFw() {
    if (fw != null) {
      fw.close()
    }
  }
}

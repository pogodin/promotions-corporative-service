import scala.annotation.tailrec

object MainApp1 extends App with RedisDao with ParseService with FileService {
  val host: String = args(0)
  val port: String = args(1)
  val dirPath: String = if (args.length > 2) args(2) else ""

  override val redisHost = host
  override val redisPort = port.toInt

  var time = System.currentTimeMillis()
  var init = false
  var refCounter = 0
  var promCounter = 0

  println("\n --------------------------------------------------------- \n Starting process..")
  mainn()
  outputCheck(true)
  println()
  println(s"Total refs: $refCounter")
  println(s"Total promos: $promCounter")
  println("Process ended. \n ---------------------------------------------------------")

  @tailrec
  def mainn(cursor: Int = 0): Unit = {
    if (cursor != 0 || !init) {
      init = true

      val refs = getAllRefs(cursor)

      refs._2.foreach(ref => {
        getPromo(ref).foreach(p => {
          val report = mkReport(ref, p)
          writeToFile(report, dirPath)
          promCounter += 1
        })
        outputCheck()
        refCounter += 1
      })

      mainn(refs._1.getOrElse(0))
    } else {
      closeFw()
    }
  }

  def outputCheck(now: Boolean = false) {
    if (System.currentTimeMillis() - time > 1000 || now) {
    time = System.currentTimeMillis()
    print(s"\rProcessed (references : promos) => ($refCounter : $promCounter)")
    }
  }
}

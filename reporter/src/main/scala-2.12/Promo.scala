case class Promo(mongoid: String,
                 cempresa: String,
                 cbarraaa: String,
                 ctallaec: String,
                 cdepartm: String,
                 cfamilia: String,
                 cdivisio: String,
                 cfabrica: String,
                 cmarmuma: String,
                 finiefec: String,
                 ffinefec: String,
                 choraini: String,
                 chorafin: String,
                 cnivelin: String,
                 cooferta: String,
                 cpromoci: String
                )
object Promo {
  def fromRedisResult(res: Seq[String]) = Promo(
    res(1),
    res(3),
    res(5),
    res(7),
    res(9),
    res(11),
    res(13),
    res(15),
    res(17),
    res(19),
    res(21),
    res(23),
    res(25),
    res(27),
    res(29),
    res(31)
  )
}
/**
  * MONGOID,
  * CEMPRESA,
  * CDEPARTM,
  * CFAMILIA,
  * CBARRAAA,
  * CTALLAEC,
  * CDIVISIO,
  * CFABRICA,
  * CMARMUMA,
  * FINIEFEC,
  * FFINEFEC,
  * CHORAINI,
  * CHORAFIN,
  * CNIVELIN,
  * COOFERTA,
  * CPROMOCI
  *
  */

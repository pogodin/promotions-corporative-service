
lazy val commonSettings = Seq(
  name := "promotions-service-reporter",
  version := "1.0",
  scalaVersion := "2.12.1"
)
lazy val app = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Seq(
      "net.debasishg" %% "redisclient" % "3.3",
      "org.scalatest" %% "scalatest" % "3.0.1" % "test",
      "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % "test"
    )
  )
lazy val commonSettings = Seq(
  name := "promotions-service-web-app",
  version := "1.0",
  scalaVersion := "2.11.7"
)

lazy val app = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= {
      val akkaVersion = "2.4.2"
      val akkaHttpVersion = "2.0.3"
      Seq(

        "com.typesafe.akka" %% "akka-actor" % akkaVersion,
        "com.typesafe.akka" %% "akka-http-core-experimental" % akkaHttpVersion,
        "com.typesafe.akka" %% "akka-http-experimental" % akkaHttpVersion,
        "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaHttpVersion,
        "net.debasishg" %% "redisclient" % "3.0",
        "io.dropwizard.metrics" % "metrics-core" % "3.1.2",

        "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
        "ch.qos.logback" % "logback-classic" % "1.1.6",
        "org.slf4j" % "jcl-over-slf4j" % "1.7.21",

        "redis.clients" % "jedis" % "2.9.0",
        "org.scalatest" %% "scalatest" % "2.2.6" % "test",
        "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test",
        "com.typesafe.akka" %% "akka-http-testkit-experimental" % akkaHttpVersion % "test"
      )
    }
  )

package manawa.promoservice

import com.redis.RedisClient

import scala.concurrent.Future

trait RedisDao extends KeysFormatter {
  dependencies: ExecContextProvider =>

  def redisHost = "localhost"

  def redisPort = 6379

  private lazy val redisClient = new RedisClient(redisHost, redisPort)

  def getRef(keys: Seq[String]): Future[Option[String]] = {
    Future {
      redisClient.get[String](formatRefKey(keys))
    }
  }

  def getPromos(keys: Seq[String], all: Boolean = true): Future[Seq[String]] = {
    Future {
      val promoKeys = formatPromoKeys(keys, all)
      redisClient.sunion[String](promoKeys.head, promoKeys.tail: _*)
        .toSeq.flatten.flatten
    }
  }
}

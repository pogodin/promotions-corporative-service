package manawa.promoservice

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import manawa.promoservice.metrics.{MeterMetricDto, MetricsDto, TimerMetricDto}
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val timerMetricDtoFormat = jsonFormat6(TimerMetricDto.apply)
  implicit val meterMetricDtoFormat = jsonFormat5(MeterMetricDto.apply)
  implicit val metricDtoFormat = jsonFormat2(MetricsDto.apply)
}

package manawa.promoservice

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.{ActorMaterializer, Materializer}
import com.codahale.metrics.MetricRegistry
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContextExecutor

object WebApp extends App
  with WebRoutesProvider
  with MetricsRegistryProvider
  with ExecContextProvider {

  implicit val system = ActorSystem()
  implicit val executor = system.dispatcher
  implicit val materializer = ActorMaterializer()

  val config = ConfigFactory.load()
  override val metrics: MetricRegistry = new MetricRegistry

  println(s"starting Promo Service on port ${config.getInt("http.port")}")
  Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
}

trait MetricsRegistryProvider {
  val metrics: MetricRegistry
}

trait ExecContextProvider {
  implicit def executor: ExecutionContextExecutor

  implicit def materializer: Materializer
}

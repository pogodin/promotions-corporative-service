package manawa.promoservice.metrics

import java.util.Comparator

import com.codahale.metrics.{Meter, Timer}
import manawa.promoservice.MetricsRegistryProvider

import scala.collection.immutable.ListMap

trait MetricsReporter {
  dependencies: MetricsRegistryProvider =>

  def getMetrics = MetricsDto(getMetricsTimers, getMetricsMeters)

  def getMetricsTimers: Map[String, TimerMetricDto] = {
    import scala.collection.convert.WrapAsScala._

    ListMap(metrics.getTimers().mapValues { t: Timer =>
      val s = t.getSnapshot
      TimerMetricDto.fromNanos(t.getCount, s.getMean, s.getMedian, s.getValue(0.9), s.get99thPercentile(),
        s.get999thPercentile()).roundDoubles(3)
    }.toSeq.sortWith { case (a1, a2) => MetricsKeysComparator.compare(a1._1, a2._1) > 0 }: _*)
  }

  def getMetricsMeters: Map[String, MeterMetricDto] = {
    import scala.collection.convert.WrapAsScala._

    ListMap(metrics.getMeters().mapValues { m: Meter =>
      MeterMetricDto(m.getCount, m.getMeanRate, m.getOneMinuteRate, m.getFiveMinuteRate, m.getFifteenMinuteRate)
        .roundDoubles(4)
    }.toSeq.sortWith { case (a1, a2) => MetricsKeysComparator.compare(a1._1, a2._1) > 0 }: _*)
  }
}

case class MetricsDto(timers: Map[String, TimerMetricDto], meters: Map[String, MeterMetricDto])

case class TimerMetricDto(
                           count: Long,
                           mean: Double,
                           median: Double,
                           perc90: Double,
                           perc99: Double,
                           perc999: Double
                         ) {
  def roundDoubles(decimalsNumber: Int): TimerMetricDto = {
    val r: Rounder = new Rounder(decimalsNumber)
    new TimerMetricDto(count, r.round(mean), r.round(median), r.round(perc90), r.round(perc99), r.round(perc999))
  }
}

object TimerMetricDto {
  def fromNanos(count: Long, mean: Double, median: Double, perc90: Double, perc99: Double, perc999: Double): TimerMetricDto = {
    def nanosToMs(nanos: Double) = nanos / 1000000
    new TimerMetricDto(count, nanosToMs(mean), nanosToMs(median), nanosToMs(perc90), nanosToMs(perc99), nanosToMs(perc999))
  }
}

case class MeterMetricDto(
                           count: Long,
                           meanRate: Double,
                           oneMinuteRate: Double,
                           fiveMinuteRate: Double,
                           fifteenMinuteRate: Double
                         ) {
  def roundDoubles(decimalsNumber: Int): MeterMetricDto = {
    val r: Rounder = new Rounder(decimalsNumber)
    new MeterMetricDto(count, r.round(meanRate), r.round(oneMinuteRate), r.round(fiveMinuteRate), r.round(fifteenMinuteRate))
  }
}

private class Rounder(decimalsNumber: Int) {
  val i: Double = Math.pow(10, decimalsNumber)

  def round(d: Double): Double = (d * i).round / i
}

private object MetricsKeysComparator extends Comparator[String] {
  private val primary = Seq("rest-", "db-", "warn-", "error-")
  private val secondary = Seq("-get", "-patch", "-post", "-put")

  def compare(o1: String, o2: String): Int = {
    if (o1 == null && o2 == null) 0
    else if (o1 == null) -1
    else if (o2 == null) 1
    else wager(o1) - wager(o2)
  }

  private def wager(str: String): Int = wager(str, primary) * secondary.size + wager(str, secondary)

  private def wager(str: String, marks: Seq[String]): Int = {
    marks.reverse.zipWithIndex.find(m => str.contains(m._1)).map(_._2).getOrElse(0)
  }
}

package manawa.promoservice

import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import spray.json.{JsArray, JsNumber, JsObject, JsString}

import scala.collection.immutable.ListMap
import scala.concurrent.Future
import scala.util.Random

trait WebRoutesProvider extends RedisDao with JsonSupport {
  dependencies: ExecContextProvider =>

  val routes = {
    logRequestResult("http-access", Logging.InfoLevel) {
      path("single" / Segment / Segment / Segment / Segment / Segment) { (p1, p2, p3, p4, p5) =>
        process(Seq(p1, p2, p3, p4, p5))
      } ~
        path("single" / Segment / Segment / Segment / Segment) { (p1, p2, p3, p4) =>
          process(Seq(p1, p2, p3, p4, ""))
        } ~
        path("test" / Segment) { s =>
          complete(s + "_" + Random.nextLong().toBinaryString)
        }
    }
  }

  def process(keys: Seq[String]): Route = {
    parameter('p.as[Int].?) { promosNum: Option[Int] =>
      onSuccess(get(keys, promosNum)) {
        case None => complete(StatusCodes.NotFound, "Not Found")
        case Some(r) => complete(r)
      }
    }
  }

  def get(keys: Seq[String], promosNum: Option[Int]): Future[Option[JsObject]] = {
    getRef(keys) flatMap {
      case None => Future.successful(None)
      case Some(dfm) =>
        val dfmArr: Array[String] = {
          val split = dfm.split(" ")
          if (split.length == 2) split :+ "" else split
        }
        if (dfmArr.length != 3) {
          Future.failed(new RuntimeException("redis ref value doesn't have 3 strings: \"" + dfm + "\""))
        } else {
          getPromos(keys ++ dfmArr, promosNum.isEmpty) map { promos =>
            val pp = promosNum.fold(promos)(promos.take)
              .map(p => JsString(p.replace("\\\"", "\"")))
//            println("found promos: " + pp)

            Some(JsObject(ListMap(
              "CEMPRESA" -> JsString(keys.head),
              "CDEPARTM" -> JsString(keys(1)),
              "CFAMILIA" -> JsString(keys(2)),
              "CBARRAAA" -> JsString(keys(3)),
              "CTALLAEC" -> JsString(keys(4)),
              "CDIVISIO" -> JsString(dfmArr(0)),
              "CFABRICA" -> JsString(dfmArr(1)),
              "CMARMUMA" -> JsString(dfmArr(2)),
              "promotionsNum" -> JsNumber(promos.size),
              "promotions" -> JsArray(pp.toVector)
            )))
          }
        }
    }
  }
}

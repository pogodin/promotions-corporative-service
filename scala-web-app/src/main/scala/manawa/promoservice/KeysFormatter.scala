package manawa.promoservice

trait KeysFormatter {
  //noinspection ScalaMalformedFormatString
  def formatRefKey(keys: Seq[String]): String = {
    if (keys.size != 5 && keys.size != 4) {
      throw new RuntimeException("can't get ref by keys number different than 4 or 5")
    }
    "r:%1$s:%2$s:%3$s:%4$s:%5$s" format ((if (keys.size == 5) keys else keys :+ ""): _*)
  }

  //noinspection ScalaMalformedFormatString
  def formatPromoKeys(keys: Seq[String], all: Boolean = true): Seq[String] = {
    if (keys.size != 8) {
      throw new RuntimeException("can't get promo by keys number different than 8")
    }

    val pattern = if (all)
      "p:%1$s:%2$s:%3$s:%4$s:%5$s:-:-:- p:%1$s:%2$s:%3$s:%4$s:-:-:-:- p:%1$s:%2$s:%3$s:-:-:-:-:%8$s p:%1$s:%2$s:-:-:-:-:-:%8$s p:%1$s:%2$s:%3$s:-:-:-:%7$s:- p:%1$s:%2$s:-:-:-:-:%7$s:- p:%1$s:%2$s:%3$s:-:-:-:-:- p:%1$s:%2$s:-:-:-:-:-:- p:%1$s:-:-:-:-:%6$s:-:- p:-:-:-:-:-:-:-:- p:%1$s:-:-:-:-:-:-:-"
    else
      "p:%1$s:%2$s:%3$s:%4$s:%5$s:-:-:- p:%1$s:%2$s:%3$s:%4$s:-:-:-:- p:%1$s:%2$s:%3$s:-:-:-:-:%8$s p:%1$s:%2$s:-:-:-:-:-:%8$s p:%1$s:%2$s:%3$s:-:-:-:%7$s:- p:%1$s:%2$s:-:-:-:-:%7$s:- p:%1$s:%2$s:%3$s:-:-:-:-:- p:%1$s:%2$s:-:-:-:-:-:- p:%1$s:-:-:-:-:%6$s:-:- p:%1$s:-:-:-:-:-:-:-"

    pattern.format(keys: _*).split(" ")
  }
}

package manawa.promoservice

import org.scalatest.{FlatSpec, FunSuite, Matchers}

class KeysFormatterTest extends FlatSpec with Matchers
  with KeysFormatter {

  "Keys Formatter" should "format keys for searching promos" in {
    formatPromoKeys(Seq("a", "b", "c", "d", "e", "f", "g", "h")) shouldBe Seq(
      "p:a:b:c:d:e:-:-:-",
        "p:a:b:c:d:-:-:-:-",
        "p:a:b:c:-:-:-:-:h",
        "p:a:b:-:-:-:-:-:h",
        "p:a:b:c:-:-:-:g:-",
        "p:a:b:-:-:-:-:g:-",
        "p:a:b:c:-:-:-:-:-",
        "p:a:b:-:-:-:-:-:-",
        "p:a:-:-:-:-:f:-:-",
        "p:-:-:-:-:-:-:-:-",
        "p:a:-:-:-:-:-:-:-"
    )
  }

  it should "format keys for searching a ref" in {
    formatRefKey(Seq("a", "b", "c", "d", "e")) shouldBe "r:a:b:c:d:e"
    formatRefKey(Seq("a", "b", "c", "d")) shouldBe "r:a:b:c:d:"
  }
}

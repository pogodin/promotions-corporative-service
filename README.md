# Promotions Corporate Service #

The Promotions Corporate Service provides the following functionality:

* import references and promotion from the legacy system dump files.
* provides a list of promotions by reference

The importing from the legacy system flow:

* dump files are copied to file system
* Nifi loads references and promotions from file system and saved to MongoDB
* Nifi loads references and promotions from MongoDB and sends to Kafka
* Spark takes references and promotions from Kafka and sends to Redis, updated migration status in MongoDB
* NodeJS application backed by Redis provides promotions by references upon request 
* alternatively, SparkWebApp can do NodeJS app's job

In order to use Promotions Corporate Service you need to

* install the moving parts according to [README_installation.md](README_installation.md)
* set up the moving parts according to [README_setting_up.md](README_setting_up.md)

Load test instruction is in file [README_loadtest.md](README_loadtest.md)

### Migration ###

References and promotions should be migrated from the legacy system dump
files to Redis using Nifi, MongoDB, Kafka and Spark

### Migration from dump files to MongoDB ###

* copy files to host the Nifi host
    * T3766000.txt for references
    * TA964700.txt for promos
* place them in different folders
    * `mkdir T37 && mv T3766000.txt T37`
    * `mkdir TA && mv TA964700.txt TA`
* split files into smaller
    ```
    cd T37
    split -dl 100000 T3766000.txt
    cd ../TA
    split -dl 10000 TA964700.txt
    cd ..
    ```
* start Nifi migration process:
    * at Nifi web interface <Nifi host>:8080/nifi/ launch files-to-MongoDB two phases
* process references with Nifi: copy them to `in` directory and Nifi will take are of them
    * `cp T37/x* /nififiles/refs/in`
    * monitor the process at Nifi web interface <Nifi host>:8080/nifi/
    * check refs count in Mongo `mongo promoservice`  `db.refs.count()`
* process promos with Nifi: copy them to `in` directory and Nifi will take are of them
    * `cp TA/x* /nififiles/promos/in`
    * monitor the process at Nifi web interface <Nifi host>:8080/nifi/
    * check refs count in Mongo `mongo promoservice`  `db.promos.count()`
* create indexes:
    * go to mongo console `mongo promoservice`
    * `db.refs.createIndex({"CEMPRESA":1, "CDEPARTM":1, "CFAMILIA":1, "CBARRAAA":1, "CTALLAEC":1, "CDIVISIO":1, "CFABRICA":1, "CMARMUMA":1})`
    * `db.refs.createIndex({"status":1, "status.code":1})`
    * `db.promos.createIndex({"status":1, "status.code":1})`

### Migration from MongoDB to Redis ###

* start Nifi MongoDB-to-Kafka two phases
* monitor the spark app logs: `tail -F ~/spark/spark.log | grep ==========`
    * or full log `tail -F ~/spark/spark.log`
* spark app will update status at MongoDB, so you can check status using MongoDB console `mongo promoservice`:
    * how many refs and promos are total: 
        * `db.refs.count()` 
        * `db.promos.count()` 
    * how many are remaining: 
        * `db.refs.count({$or:[{status:{$exists:false}}, {"status.done":{$ne:"done"}}]})` 
        * `db.promos.count({$or:[{status:{$exists:false}}, {"status.done":{$ne:"done"}}]})`
    * how many migrated: 
        * `db.refs.count({"status.done":"done"})` 
        * `db.promos.count({"status.done":"done"})` 
* you can check status using Redis console `redis-cli` or `redis-cli -h 172.31.19.49`:
    * total number of entries: `dbsize`
    * memory consumed: `INFO Memory`

### Using web app: getting promos by refs ###

NodeJS app contains of 4 files: app.js, config.js, main.js, package.json.
The file config.js contains the entry `config.redis.promokeytemplates`.
It provdes the template for extracting promos from Redis.
If you don't want to extract "all refs promos", this line should not contain `p:-:-:-:-:-:-:-:-`

##### Usage #####

`GET` http request: `<host>:3000/single/a/b/c/d/e` or `<host>:3000/single/a/b/c/d` (means `e` is an empty line) 
where `a-e` are the fields of the reference requested:

* `a` stands for CEMPRESA
* `b` stands for CDEPARTM
* `c` stands for CFAMILIA
* `d` stands for CBARRAAA
* `e` stands for CTALLAEC

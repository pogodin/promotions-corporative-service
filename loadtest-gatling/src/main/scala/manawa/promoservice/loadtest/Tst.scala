package manawa.promoservice.loadtest

object Tst extends App with HttpParamsStore {
  if (args.length != 1) {
    println("specify redis url")
    System.exit(1)
  }
  val redisUrl = args(0)

  loadHttpParamsFromRedisRefKeys(1000000, redisUrl)

  println(s"quit in: ")
  for (i <- 20 to 1 by -1) {
    println(f"$i%6d   random key: $randomHttpParams")
    Thread.sleep(1000)
  }
}

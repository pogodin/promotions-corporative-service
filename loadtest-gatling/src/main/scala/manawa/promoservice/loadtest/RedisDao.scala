package manawa.promoservice.loadtest

import com.redis.RedisClient

import scala.annotation.tailrec
import scala.collection.immutable.Nil
import scala.collection.mutable.ListBuffer
import scala.util.Random

class RedisDao(redisHost: String = "localhost", redisPort: Int = 6379) {
  private lazy val redisClient = new RedisClient(redisHost, redisPort)

  def getKeys(pattern: String): List[String] = {
    redisClient.keys(pattern) map (_.flatten) getOrElse Nil
  }

  def exists(pattern: String): Boolean = redisClient.exists(pattern)

  def scanKeys(pattern: String, keysFilter: String => Boolean, num: Int): List[String] = {
    def scan(cursor: Int): (Option[Int], List[String]) = {
      redisClient.scan(cursor, pattern, 1000) match {
        case Some((nextCursorOpt, optItemsOpt)) =>
          (nextCursorOpt.flatMap(c => if (c == 0) None else Some(c)), optItemsOpt.toList.flatten.flatten)
        case None => (None, Nil)
      }
    }

    @tailrec
    def scanIteration(cursorOpt: Option[Int], list: ListBuffer[String], num: Int): ListBuffer[String] = {
      cursorOpt match {
        case None => list
        case Some(cursor) =>
          val tuple: (Option[Int], List[String]) = scan(cursor)
          val (nextCursorOpt, extracted) = (tuple._1, tuple._2.filter(keysFilter))
          if (extracted.size >= num) {
            list ++ extracted.take(num)
          } else {
            scanIteration(nextCursorOpt, list ++ extracted, num - extracted.size)
          }
      }
    }

    scanIteration(Some(0), ListBuffer.empty, num).toList
  }
}

object RedisDao extends App {
  val usage = "usage: keys <patter> <num> OR scan <pattern> <num>"
  if (args.length != 3) {
    println(usage)
    System.exit(1)
  }
  val Array(command, pattern, numStr) = args
  val num = numStr.toInt

  val loadKeys: (RedisDao => List[String]) = {
    if (command == "keys") {
      {_.getKeys(pattern).take(num)}
    } else if (command == "scan") {
      {_.scanKeys(pattern, _ => true, num)}
    } else {
      throw new RuntimeException(s"unknown command $command. $usage")
    }
  }

  def memoryConsumed = (Runtime.getRuntime.totalMemory - Runtime.getRuntime.freeMemory) / 1024
  println(s"totalMem = ${Runtime.getRuntime.totalMemory / 1024} kb, freeMem = ${Runtime.getRuntime.freeMemory / 1024}")
  val mem = memoryConsumed
  val keys: List[String] = loadKeys(new RedisDao())
  println(s"totalMem = ${Runtime.getRuntime.totalMemory / 1024} kb, freeMem = ${Runtime.getRuntime.freeMemory / 1024}")
  println(s"loaded ${keys.size} keys. They took ${memoryConsumed - mem} kB of memory")

  println(s"quit in: ")
  for (i <- 15 to 1 by -1) {
    println(s"$i " + keys(Random.nextInt(keys.size)))
    Thread.sleep(1000)
  }
}
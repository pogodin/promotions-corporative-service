package manawa.promoservice.loadtest

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

/**
  * usage:
  * java -cp gatling-loadtest-for-promotions-service-assembly-1.0.jar -Dusers=4000 -Dramp=60 -Dpause=1500 -Dcalls=100 -Dport=3000 io.gatling.app.Gatling -s manawa.promoservice.loadtest.PromoSimulation
  * java -cp gatling-loadtest-for-promotions-service-assembly-1.0.jar -Durl=172.31.19.49 -Dredis=172.31.19.49 -Dusers=1000 -Dramp=30 -Dpause=50 -Dcalls=100 io.gatling.app.Gatling -s manawa.promoservice.loadtest.PromoSimulation
  */
class PromoSimulation extends Simulation with HttpParamsStore {
  private val webAppUrl = {
    var url: String = System.getProperty("url", "localhost")
    val port: Int = Integer.getInteger("port", 3000)
    if (!url.contains(":")) {
      url = url + ":" + port
    }
    "http://" + url
  }
  private val redisUrl = System.getProperty("redis", "localhost")
  private val usersN: Int = Integer.getInteger("users", 100)
  private val rampSec: Int = Integer.getInteger("ramp", 20)
  private val callsN: Int = Integer.getInteger("calls", 30)
  private val pauseMs: Int = Integer.getInteger("pause", 100)

  println("=========== Configuration ===========")
  println(s"web app url = $webAppUrl")
  println(s"redis url = $redisUrl")
  println(s"users number = $usersN")
  println(s"user are added during $rampSec seconds")
  println(s"call number per user = $callsN")
  println(s"pause between calls = $pauseMs ms")
  println("=====================================")

  val httpConf = http.baseURL(webAppUrl)

  val scn = scenario("Single Ref")
    .repeat(callsN) {
      feed(randomKeysFeeder)
        .exec(http("single_ref_by_random_key")
          .get("/single/${randomHttpParams}?p=2")
        )
        .pause(pauseMs.millis)
    }

  setUp(
    scn.inject(
      if (rampSec > 0) rampUsers(usersN) over (rampSec seconds)
      else atOnceUsers(usersN)
    ).protocols(httpConf)
  )

  private lazy val randomKeysFeeder: Feeder[String] = {
    loadHttpParamsFromRedisRefKeys(100000, redisUrl)
    Iterator.continually(Map("randomHttpParams" -> randomHttpParams))
  }

  // TODO: remove non-DRY
  private lazy val someKeysFeeder = Array(
    Map("randomHttpParams" -> "19/2/1/19808/"),
    Map("randomHttpParams" -> "2/83/700/1268/40"),
    Map("randomHttpParams" -> "2/62/601/1479/478"),
    Map("randomHttpParams" -> "2/23/739/378/"),
    Map("randomHttpParams" -> "28/582/400/818/3"),
    Map("randomHttpParams" -> "2/55/301/5492/"),
    Map("randomHttpParams" -> "19/12/701/25345/"),
    Map("randomHttpParams" -> "2/62/601/588/523"),
    Map("randomHttpParams" -> "19/16/924/6463/"),
    Map("randomHttpParams" -> "2/117/307/28844/"),
    Map("randomHttpParams" -> "28/562/51/8535/42"),
    Map("randomHttpParams" -> "2/92/631/4839/3"),
    Map("randomHttpParams" -> "2/62/600/5662/452"),
    Map("randomHttpParams" -> "2/81/852/10930/40"),
    Map("randomHttpParams" -> "19/12/86/38690/"),
    Map("randomHttpParams" -> "2/82/604/1420/28"),
    Map("randomHttpParams" -> "27/180/24/317/"),
    Map("randomHttpParams" -> "2/86/835/261/4"),
    Map("randomHttpParams" -> "19/902/916/8405/"),
    Map("randomHttpParams" -> "19/902/903/19186/")
  ).random
}

package manawa.promoservice.loadtest

import java.util.concurrent.ThreadLocalRandom

import scala.collection.immutable.ListMap

trait HttpParamsStore {
  private[loadtest] var keys: List[String] = List.empty

  def loadHttpParamsFromRedisRefKeys(num: Int, redisHost: String = "localhost", redisPort: Int = 6379): Int = {
    val dao: RedisDao = new RedisDao(redisHost, redisPort)

    val keyPatterns: Map[String, Int] = ListMap(
      "[^1-2]" -> num / 10 * 3,
      "[^1]"  -> num / 10 * 3,
      "1" -> num / 10 * 4
    ) map (kv => "r:" + kv._1 + "*" -> kv._2)

    val regex = "r:[^\\:]+:[^\\:]+:[^\\:]+:[^\\:]+:[^\\:]*".r.pattern
    val hasFiveColons: String => Boolean = regex.matcher(_).matches

    /**
      * converts "r:1:2:509:289:729" into 1/2/509/289/729
      * @param key redis key
      * @return http params
      */
    def redisKeyToHttpUrlParams(key: String) = key.substring(2).replace(':', '/')

    def totalMem = Runtime.getRuntime.totalMemory / 1024 / 1024
    def freeMem = Runtime.getRuntime.freeMemory / 1024 / 1024
    def memoryConsumed = totalMem - freeMem
    def printMem() = println(s"totalMem = $totalMem Mb, freeMem = $freeMem Mb")

    val mem = memoryConsumed
    printMem()

    println(s" pre-loading refs keys...")
    val preLoadedRefsKeys: List[String] = keyPatterns.flatMap { case (keyPattern, n) =>
      print(s"    pre-loading keys for pattern $keyPattern ...")
      val ks = dao.scanKeys(keyPattern, hasFiveColons, n).map(redisKeyToHttpUrlParams)
      println(s"\t Loaded ${ks.size}")
      ks
    }.toList

    printMem()
    println(s" pre-loaded ${preLoadedRefsKeys.size} refs keys. They took ${memoryConsumed - mem} MB of memory")

    keys = preLoadedRefsKeys
    preLoadedRefsKeys.size
  }

  def randomHttpParams: String = keys(ThreadLocalRandom.current().nextInt(keys.size))
}

object ListKeysStoreTst extends App {
  if (args.length != 1) {
    println("specify number of keys")
    System.exit(1)
  }

  val keysNum = args(0).toInt

  val httpParamsStore = new HttpParamsStore() {}
  httpParamsStore.loadHttpParamsFromRedisRefKeys(keysNum)
}

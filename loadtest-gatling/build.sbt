enablePlugins(GatlingPlugin)

scalaVersion := "2.11.8"

name := "gatling-loadtest-for-promotions-service"

version := "1.0"

scalacOptions := Seq(
  "-encoding", "UTF-8", "-target:jvm-1.8", "-deprecation",
  "-feature", "-unchecked", "-language:implicitConversions", "-language:postfixOps")

// weird, but required to resolve conflict of property files in netty
assemblyMergeStrategy in assembly <<= (assemblyMergeStrategy in assembly) { (old) =>
  {
    case PathList(ps @ _*) if ps.last endsWith "io.netty.versions.properties" => MergeStrategy.first
    case x =>
      //val old = (mergeStrategy in assembly).value
      old(x)
  }
}

libraryDependencies += "net.debasishg" %% "redisclient" % "3.0"
libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.2.2"
libraryDependencies += "io.gatling" % "gatling-test-framework" % "2.2.2"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"
lazy val commonSettings = Seq(
  name := "promotions-service-spark-streaming",
  version := "1.0",
  scalaVersion := "2.10.6"
)

lazy val app = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-streaming" % "1.6.2" % "provided",
      "org.apache.spark" %% "spark-sql" % "1.6.2" % "provided",
      ("org.apache.spark" %% "spark-streaming-kafka" % "1.6.2")
        .exclude("org.spark-project.spark", "unused"),
      "com.stratio.datasource" %% "spark-mongodb" % "0.11.2",
      "org.mongodb" %% "casbah" % "3.1.1",
      "org.json4s" %% "json4s-native" % "3.2.10",
      "redis.clients" % "jedis" % "2.9.0",
      "org.scalatest" %% "scalatest" % "2.0" % "test",
      "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test"
    )
  )

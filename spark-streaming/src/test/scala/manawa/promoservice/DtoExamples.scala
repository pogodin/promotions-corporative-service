package manawa.promoservice

import com.mongodb.{BasicDBObject, DBObject}

trait DtoExamples {
  val promo1fromMongo =
    """{ "_id" : ObjectId("5765f9161f5acf187def583d"), "CEMPRESA" : "001", "CDEPARTM" : "0134",""" +
      """ "CFAMILIA" : "665", "CBARRAAA" : "02707", "CTALLAEC" : "003", "CDIVISIO" : "05", "CFABRICA" : "0000000",""" +
      """ "CMARMUMA" : "000000000000000", "FINIEFEC" : "20140407", "FFINEFEC" : "20180130", "CHORAINI" : "0000",""" +
      """ "CHORAFIN" : "2359", "CNIVELIN" : "7", "COOFERTA" : "99900000375808", "CPROMOCI" : "14079013",""" +
      """ "status" : { "code" : "in-progress", "updated" : ISODate("2016-06-19T22:21:56.873Z") } }"""

  val promo2fromKafka =
    """{ "_id" : { "$oid" : "e20a6f35de1ee8f4sa8fcb2a" }, "CEMPRESA" : "001", "CDEPARTM" : "0134",""" +
      """ "CFAMILIA" : "665", "CBARRAAA" : "02707", "CTALLAEC" : "004", "CDIVISIO" : "01", "CFABRICA" : "0000000",""" +
      """ "CMARMUMA" : "PAUL AND SHARK ", "FINIEFEC" : "20150603", "FFINEFEC" : "20191021", "CHORAINI" : "0000",""" +
      """ "CHORAFIN" : "2198", "CNIVELIN" : "3", "COOFERTA" : "99900000375926", "CPROMOCI" : "14079013" }"""

  val promo1obj = Promotion("5765f9161f5acf187def583d", "001", "0134", "665", "02707", "003", "05", "0000000",
    "000000000000000", "20140407", "20180130", "0000", "2359", "7", "99900000375808", "14079013")
  val promo2obj = Promotion("e20a6f35de1ee8f4sa8fcb2a", "001", "0134", "665", "02707", "004", "01", "0000000",
    "PAUL AND SHARK ", "20150603", "20191021", "0000", "2198", "3", "99900000375926", "14079013")
  val promo3obj = Promotion("e20a6f35de1ee8f4sa8fcb2a", "001", "0134", "  665", " % ", "  004  ", "01", "0000000",
    "PAUL AND SHARK ", "20150603", "20191021", "0000", "2198", "0", "99900000375926", "14079013")

  val promo1json =
    """{"MONGOID":"5765f9161f5acf187def583d","CEMPRESA":"001","CDEPARTM":"0134","CFAMILIA":"665","CBARRAAA":"02707",""" +
      """"CTALLAEC":"003","CDIVISIO":"05","CFABRICA":"0000000","CMARMUMA":"000000000000000","FINIEFEC":"20140407",""" +
      """"FFINEFEC":"20180130","CHORAINI":"0000","CHORAFIN":"2359","CNIVELIN":"7","COOFERTA":"99900000375808",""" +
      """"CPROMOCI":"14079013"}"""

  val promo1fixedFormat = "20140407 20180130 0000 2359 99900000375808 14079013"
  val promo2fixedFormat = "20150603 20191021 0000 2198 99900000375926 14079013"

  val ref1fromKafka =
    """{ "_id" : { "$oid" : "57736aa9ad7afc0454c57908" }, "CEMPRESA" : "01", "CDEPARTM" : "134",""" +
      """ "CFAMILIA" : "665", "CBARRAAA" : "02707", "CTALLAEC" : "003", "CDIVISIO" : "04",""" +
      """ "CFABRICA" : "0972679", "CMARMUMA" : "FREE STYLE     " }"""

  val ref1obj = Reference("57736aa9ad7afc0454c57908", "01", "134", "665", "02707", "003", "04", "0972679", "FREE STYLE     ")
  val ref2obj = Reference("57736aa9ad7a6ad9238e49bc", "01", "134", "241", "52455", "034", "07", "5865312", "NONFREE STYLE  ")
  val ref3obj = Reference("57736aa9ad7a6ad9287be01b", "01", "134", "160", "00000", "066", "08", "3473462", "WEIRD STYLE    ")
  val ref4obj = Reference("a87b0e00c083d491ad024bc5", "01", " 034", "5 ", "00000", " 00066", "  &  ", " A B C D  ", "WEIRD STYLE    ")

  val ref1props = "04 0972679 FREE STYLE"
  val ref2props = "07 5865312 NONFREE STYLE"
  val ref3props = "08 3473462 WEIRD STYLE"

  def mongoObj(r: Reference): DBObject = mongoObj(mkFields(r))

  def mongoObj(fields: Map[String, String]): DBObject = {
    fields.foldLeft(new BasicDBObject()) { case (o, e) => o.append(e._1, e._2) }
  }

  def mkFields(o: Reference): Map[String, String] = {
    val refFieldKeys = Seq("CEMPRESA", "CDEPARTM", "CFAMILIA", "CBARRAAA", "CTALLAEC", "CDIVISIO", "CFABRICA", "CMARMUMA")
    refFieldKeys.zip(Reference.unapply(o).get.productIterator.toList.asInstanceOf[List[String]]).toMap
  }
}

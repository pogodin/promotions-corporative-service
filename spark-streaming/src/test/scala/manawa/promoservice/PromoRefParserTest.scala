package manawa.promoservice

import org.scalatest.{FlatSpec, Matchers}

class PromoRefParserTest extends FlatSpec with Matchers
  with PromoRefParser with DtoExamples {

  "promo json parser" should "parse string from mongo as Promotion" in {
    parsePromotion(promo1fromMongo).get shouldBe promo1obj
  }

  it should "parse string from kafka as Promotion" in {
    parsePromotion(promo2fromKafka).get shouldBe promo2obj
  }

  it should "serialize promotion as json string" in {
    promoAsJsonString(promo1obj) shouldBe promo1json
  }

  "ref json parser" should "parse string from kafka as Reference" in {
    parseRef(ref1fromKafka).get shouldBe ref1obj
  }

  "promo fixed-formatter" should "create fixed format from a Promotion" in {
    promoAsFixedFormat(promo1obj) shouldBe promo1fixedFormat
    promoAsFixedFormat(promo2obj) shouldBe promo2fixedFormat
  }

  "ref props fixed-formatter" should "create fixed format for ref properties" in {
    refPropsAsFixedFormat(ref1obj) shouldBe ref1props
    refPropsAsFixedFormat(ref2obj) shouldBe ref2props
    refPropsAsFixedFormat(ref3obj) shouldBe ref3props
  }
}

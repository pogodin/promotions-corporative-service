package manawa.promoservice.dao

import org.apache.spark.sql.SQLContext
import org.scalatest.{FlatSpec, Matchers}

class SparkMongoTest extends FlatSpec with Matchers {
  def sparkMongo = new SparkMongo(null) {
    override private[dao] lazy val sqlContext: SQLContext = null
  }

  "spark mongo" should "create sql" in {
    sparkMongo.createSql(RefsQuery.all) shouldBe
      "SELECT CEMPRESA, CDEPARTM, CFAMILIA, CBARRAAA, CTALLAEC, CDIVISIO, CFABRICA, CMARMUMA FROM refs"
    sparkMongo.createSql(RefsQuery.byCempresa("001").andCdepartm("0053")) shouldBe
      "SELECT CEMPRESA, CDEPARTM, CFAMILIA, CBARRAAA, CTALLAEC, CDIVISIO, CFABRICA, CMARMUMA FROM refs " +
        "WHERE CEMPRESA=\"001\" AND CDEPARTM=\"0053\""
  }
}

package manawa.promoservice.dao

import org.scalatest.{FlatSpec, Ignore, Matchers}

@Ignore // requires mongo server instance
class MongoDaoTest extends FlatSpec with Matchers
  with MongoDao {
  override def mongoHost = "localhost"

  "mongo refs dao" should "get refs by CEMPRESA, CDEPARTM, CFABRICA" in {
    val refs = getRefs(RefsQuery.byCempresa("01").andCdepartm("002").andCfabrica("0972679"))
    refs foreach {ref =>
      println(ref)
    }
  }

  "mongo dao" should "update status of multiple documents" in {
    updateStatusMulti(refsDb, Seq("576851801f5acf187def58f7", "576851811f5acf187def58f8"),"done", Some("yo-yo"))
      .get shouldBe 2
  }
}

package manawa.promoservice.dao

import manawa.promoservice.{DtoExamples, Reference}
import org.scalatest.{FlatSpec, FunSuite, Matchers}

class RedisKeyTest extends FlatSpec with Matchers with DtoExamples {
  "RefRedisKey" should "create key from a ref" in {
    RefRedisKey.fromRef(ref4obj).toString shouldBe "r:1:34:5:0:66"
  }

  "PromoRedisKey" should "create key from promo" in {
    PromoRedisKey.fromPromo(promo3obj).toString shouldBe "p:1:134:665:%:4:-:-:-"
    PromoRedisKey.fromPromo(promo3obj.copy(cnivelin = "2")).toString shouldBe "p:1:134:665:-:-:-:-:PAULANDSHARK"
  }
}

package manawa.promoservice

import com.mongodb.DBObject
import manawa.promoservice.dao.{MongoDao, RedisDao, RefsQuery}
import org.scalamock.scalatest.MockFactory
import org.scalatest.Suite

import scala.util.Try

trait MongoDaoMock extends MongoDao with MockFactory {
  dependencies: Suite =>

  protected val mongoDaoMock = mock[MongoDao]

  override def mongoHost: String = ""

  override def updatePromoStatus(id: String, statusCode: String, statusMsg: Option[String]): Try[Boolean] = {
    mongoDaoMock.updatePromoStatus(id, statusCode, statusMsg)
  }

  /**
    * get refrences by cempresa and other fields <br/>
    * Usage: {{{ getRefs(byCempresa("01")) }}}
    * OR {{{ getRefs(byCempresa("01").andCdepartm("002").andCfabrica("0972679")) }}}
    *
    * @param q query
    * @return Mongo cursor
    */
  override def getRefs(q: RefsQuery): Stream[DBObject] = mongoDaoMock.getRefs(q)
}

trait RedisDaoMock extends RedisDao with MockFactory {
  dependencies: Suite =>

  override def redisHostsAndPorts: List[(String, Int)] = List(("", 0))

  protected val redisDaoMock = mock[RedisDao]
}

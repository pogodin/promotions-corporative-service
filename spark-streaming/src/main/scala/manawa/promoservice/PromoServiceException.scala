package manawa.promoservice

class PromoServiceException(msg: String, cause: Throwable) extends Exception(msg, cause) {
  def this(msg: String) = this(msg, null)
}

class PromoServiceMultiException(val es: List[Throwable]) extends {
} with Exception({
  val sb = new StringBuilder(s"Multiple exceptions (${es.size}):")

  if (sb.nonEmpty) {
    val deduped: Map[String, Int] = es.groupBy(_.getMessage).mapValues(_.length)

    if (deduped.size > 1) {
      deduped.foreach { case (msg, num) =>
        sb.append("\n")
        if (num > 1) {
          sb.append(s"{$num}: ")
        }
        sb.append(msg)
      }
    } else {
      sb.append(deduped.head._1)
    }
  }

  sb.toString
})

class PromoStatusUpdateException(promo: Promotion, status: String, cause: Throwable)
  extends Exception(s"""Error updating mongo with promo status "$status" for $promo\nError msg: ${cause.getMessage}""", cause)

class RefStatusUpdateException(ref: Reference, status: String, cause: Throwable)
  extends Exception(s"""Error updating mongo with ref status "$status" for $ref\nError msg: ${cause.getMessage}""", cause)
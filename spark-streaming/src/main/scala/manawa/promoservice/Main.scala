package manawa.promoservice

import manawa.promoservice.dao.{MongoDao, RedisDao}
import redis.clients.jedis.HostAndPort

object Main extends App with Serializable {
  lazy val Array(kafkaBrokers, mongoHostUrl, redisHostAndPortsUrl) = args

  if (args.length == 3) {
    new SparkProcessor
      with KafkaStreamProvider
      with MongoDao
      with RedisDao
      with PromoRefParser {
      override val mongoHost = mongoHostUrl

      override def redisHostsAndPorts: List[(String, Int)] = redisHostAndPortsUrl.split(",").toList.map(toHostAndPort)
    }.startApp(kafkaBrokers)
  } else {
    println("usage: kafka-broker1-host:port,kafka-broker2-host:port mongoHost redisHost")
  }

  private def toHostAndPort(str: String): (String, Int) = {
    val colonPos = str.indexOf(":")
    if (colonPos == -1) {
      (str, 6379)
    } else {
      val (host, p) = str.splitAt(colonPos)
      (host, p.substring(1).toInt)
    }
  }
}




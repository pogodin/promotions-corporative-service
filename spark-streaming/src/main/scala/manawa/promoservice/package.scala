package manawa

import scala.annotation.tailrec

package object promoservice {
  def combineExceptions(e1: Throwable, e2: Throwable) = new PromoServiceMultiException((e1, e2) match {
    case (pe1: PromoServiceMultiException, pe2: PromoServiceMultiException) => pe1.es ::: pe2.es
    case (pe: PromoServiceMultiException, e: Throwable) => pe.es :+ e
    case (e: Throwable, pe: PromoServiceMultiException) => e :: pe.es
    case (e1: Throwable, e2: Throwable) => List(e1, e2)
  })

  @tailrec
  def removeLeadingZeros(s: String): String = {
    if (s.length > 1 && s.startsWith("0")) removeLeadingZeros(s.tail)
    else s
  }
}

package manawa.promoservice

import kafka.serializer.StringDecoder
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka.KafkaUtils

trait KafkaStreamProvider {
  def createMessagesStream(ssc: StreamingContext, kafkaBrokers: String, kafkaTopics: String): InputDStream[(String, String)] = {
    KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
      ssc,
      kafkaParams = Map[String, String]("metadata.broker.list" -> kafkaBrokers),
      topics = kafkaTopics.split(",").toSet
    )
  }
}

package manawa.promoservice.dao

case class RefsQuery(fields: Map[String, String]) {
  val andCdepartm = add("CDEPARTM") _
  val andCfamilia = add("CFAMILIA") _
  val andCbarraaa = add("CBARRAAA") _
  val andCtallaec = add("CTALLAEC") _
  val andCdivisio = add("CDIVISIO") _
  val andCfabrica = add("CFABRICA") _
  val andCmarmuma = add("CMARMUMA") _

  private def add(mongoField: String)(fieldValue: String) = RefsQuery(fields + (mongoField -> fieldValue))
}

object RefsQuery {
  val all = RefsQuery(Map.empty)
  val byCempresa = RefsQuery(Map.empty).add("CEMPRESA") _
}
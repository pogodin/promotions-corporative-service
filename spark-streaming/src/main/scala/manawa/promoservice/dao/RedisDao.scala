package manawa.promoservice.dao

import manawa.promoservice._
import redis.clients.jedis._

import scala.util.Try

trait RedisDao extends PromoRefParser {
  def redisHostsAndPorts: List[(String, Int)]

  private lazy val redisClient: JedisCommands = {
    val hostAndPorts: Set[HostAndPort] = redisHostsAndPorts.map { case (host, port) =>
      new HostAndPort(host, port)
    }.toSet

    if (hostAndPorts.isEmpty) {
      throw new RuntimeException("Redis hosts ans ports not specified")
    }
    if (hostAndPorts.size == 1) {
      new Jedis(hostAndPorts.head.getHost, hostAndPorts.head.getPort)
    } else {
      import scala.collection.convert.WrapAsJava._
      new JedisCluster(hostAndPorts)
    }
  }

  def savePromoToRedis(promo: Promotion): Try[Boolean] = {
    Try {
      redisClient.sadd(PromoRedisKey.fromPromo(promo).toString, promoAsJsonString(promo))
      true
    }
  }

  def saveRefsToRedis(refs: Seq[Reference]): Try[Seq[String]] = {
    Try {
      redisClient match {
        case c: MultiKeyCommands =>
          c.mset(refs flatMap (ref => Seq(createRefKey(ref), createRefValue(ref))): _*)
        case _ =>
          refs foreach {ref => redisClient.set(createRefKey(ref), createRefValue(ref))}
      }
      refs.map(_.mongoid)
    }
  }

  def saveRefToRedis(ref: Reference): Try[Int] = {
    Try {
      redisClient.set(createRefKey(ref), createRefValue(ref))
      1
    }
  }

  private def createRefKey(r: Reference) = RefRedisKey.fromRef(r).toString

  private def createRefValue(r: Reference): String = {
    import manawa.promoservice.removeLeadingZeros
    List(r.cdivisio, r.cfabrica, r.cmarmuma).map(s => removeLeadingZeros(s.replaceAll("\\s+", ""))).mkString(" ")
  }
}

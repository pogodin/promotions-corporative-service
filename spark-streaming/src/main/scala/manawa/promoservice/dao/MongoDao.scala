package manawa.promoservice.dao

import java.util.Date

import com.mongodb.casbah.Imports._
import org.bson.types.ObjectId

import scala.util.Try

trait MongoDao {
  def mongoHost: String

  def mongoPort = 27017

  lazy val db: MongoDB = {
    val mongoClient = MongoClient(mongoHost, mongoPort)
    mongoClient("promoservice")
  }

  lazy val promoDb: MongoCollection = db("promos")
  lazy val refsDb: MongoCollection = db("refs")

  def getNewOrFailPromos = promoDb.find($or("status" $exists false, "status.code" $eq "fail"))

  def getNewPromos = promoDb.find("status" $exists false)

  def getFailPromos = promoDb.find("status.code" $eq "fail")

  def isAlreadyDone(mongoid: String): Try[Boolean] = {
    Try {
      promoDb.find(DBObject("_id" -> new ObjectId(mongoid), "status.code" -> "done")).size == 1
    }
  }

  def updatePromoStatus(id: String, statusCode: String, statusMsg: Option[String] = None): Try[Boolean] = {
    Try {
      promoDb.update(
        DBObject("_id" -> new ObjectId(id)),
        $set("status" -> {
          val builder = DBObject.newBuilder[String, Any] += ("code" -> statusCode)
          statusMsg filter (_.nonEmpty) foreach (msg => builder += ("msg" -> msg))
          builder += ("updated" -> new Date())
          builder.result()
        })
      ).getN == 1
    }
  }

  def updateRefStatus(id: String, statusCode: String, statusMsg: Option[String] = None): Try[Boolean] = {
    Try {
      refsDb.update(
        DBObject("_id" -> new ObjectId(id)),
        $set("status" -> {
          val builder = DBObject.newBuilder[String, Any] += ("code" -> statusCode)
          statusMsg filter (_.nonEmpty) foreach (msg => builder += ("msg" -> msg))
          builder += ("updated" -> new Date())
          builder.result()
        })
      ).getN == 1
    }
  }

  def updateStatusMulti(coll: MongoCollection, ids: Seq[String], statusCode: String,
                        statusMsg: Option[String] = None): Try[Int] = {
    Try {
      refsDb.update(
        DBObject("_id" -> DBObject("$in" -> ids.toList.map(id => new ObjectId(id)))),
        $set("status" -> {
          val builder = DBObject.newBuilder[String, Any] += ("code" -> statusCode)
          statusMsg filter (_.nonEmpty) foreach (msg => builder += ("msg" -> msg))
          builder += ("updated" -> new Date())
          builder.result()
        }),
        upsert = false,
        multi = true
      ).getN
    }
  }

  def getAllRefs: Stream[DBObject] = getRefs(RefsQuery(Map.empty))

  def getRefs(q: RefsQuery): Stream[DBObject] = refsDb.find(DBObject(q.fields.toList), DBObject("_id" -> 0)).toStream

  def countRefs(q: RefsQuery): Int = refsDb.count(DBObject(q.fields.toList))
}



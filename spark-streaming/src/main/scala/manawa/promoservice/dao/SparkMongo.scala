package manawa.promoservice.dao

import com.stratio.datasource.mongodb._
import com.stratio.datasource.mongodb.config.MongodbConfig._
import com.stratio.datasource.mongodb.config._
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SQLContext}

class SparkMongo(sc: SparkContext) {
  private[dao] lazy val sqlContext = {
    val ctx = new SQLContext(sc)
    val refsCfg = MongodbConfigBuilder(Map(
      Host -> List("localhost:27017"),
      Database -> "promoservice",
      Collection -> "refs",
      SamplingRatio -> 1.0,
      WriteConcern -> "normal"
    )).build
    val refsRdd: DataFrame = ctx.fromMongoDB(refsCfg)
    refsRdd.registerTempTable("refs")
    ctx
  }

  def refsDataFrame(query: RefsQuery): DataFrame = {
    sqlContext.sql(createSql(query))
  }

  private[dao] def createSql(query: RefsQuery): String = {
    "SELECT CEMPRESA, CDEPARTM, CFAMILIA, CBARRAAA, CTALLAEC, CDIVISIO, CFABRICA, CMARMUMA FROM refs" + {
      if (query == RefsQuery.all) ""
      else " WHERE " + query.fields.map { case (name, value) => name + "=\"" + value + "\"" }.mkString(" AND ")
    }
  }
}

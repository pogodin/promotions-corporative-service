package manawa.promoservice.dao

import manawa.promoservice._

abstract class RedisKey(prefix: String) {
  private def fieldValues: Seq[String] = {
    getClass.getDeclaredFields.toSeq map { f => f.setAccessible(true); f.get(this).toString }
  }

  override def toString: String = {
    prefix + ":" + fieldValues.map(fieldValue => removeLeadingZeros(fieldValue.replaceAll("\\s+", ""))).mkString(":")
  }
}

case class RefRedisKey(cempresa: String, cdepartm: String, cfamilia: String, cbarraaa: String, ctallaec: String)
  extends RedisKey("r")

object RefRedisKey {
  def fromRef(ref: Reference) = RefRedisKey(ref.cempresa, ref.cdepartm, ref.cfamilia, ref.cbarraaa, ref.ctallaec)
}

case class PromoRedisKey(cempresa: String = "-", cdepartm: String = "-", cfamilia: String = "-", cbarraaa: String = "-",
                         ctallaec: String = "-", cdivisio: String = "-", cfabrica: String = "-", cmarmuma: String = "-")
  extends RedisKey("p")

object PromoRedisKey {
  def fromPromo(p: Promotion): PromoRedisKey = {
    p.cnivelin match {
      case "0" => PromoRedisKey(p.cempresa, p.cdepartm, p.cfamilia, p.cbarraaa, p.ctallaec)
      case "1" => PromoRedisKey(p.cempresa, p.cdepartm, p.cfamilia, p.cbarraaa)
      case "2" => PromoRedisKey(p.cempresa, p.cdepartm, p.cfamilia, cmarmuma = p.cmarmuma)
      case "3" => PromoRedisKey(p.cempresa, p.cdepartm, cmarmuma = p.cmarmuma)
      case "4" => PromoRedisKey(p.cempresa, p.cdepartm, p.cfamilia, cfabrica = p.cfabrica)
      case "5" => PromoRedisKey(p.cempresa, p.cdepartm, cfabrica = p.cfabrica)
      case "6" => PromoRedisKey(p.cempresa, p.cdepartm, p.cfamilia)
      case "7" => PromoRedisKey(p.cempresa, p.cdepartm)
      case "8" => PromoRedisKey(p.cempresa, cdivisio = p.cdivisio)
      case "9" => PromoRedisKey()
      case "M" => PromoRedisKey(p.cempresa)
      case _ => throw new PromoServiceException(s"invalid SNIVELIN: ${p.cnivelin} for $p")
    }
  }
}

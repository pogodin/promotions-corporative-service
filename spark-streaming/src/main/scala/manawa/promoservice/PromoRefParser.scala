package manawa.promoservice

import org.json4s.ParserUtil.ParseException
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization

import scala.util.{Failure, Success, Try}

trait PromoRefParser {

  private val replaceIdRegex1 = """\{\s*"_id"\s*:\s*ObjectId\("(\w+)"\)\s*,""".r
  private val replaceIdRegex2 = """\{\s*"_id"\s*:\s*\{\s*"\$oid"\s*:\s*"(\w+)"\s*\}\s*,""".r
  private val replaceIsoDateRegex = """ISODate\("(2016-06-19T22:21:56.873Z)"\)""".r

  def parsePromotion(jsonAsString: String): Try[Promotion] = parseCaseClass[Promotion](jsonAsString)

  def parseRef(jsonAsString: String): Try[Reference] = parseCaseClass[Reference](jsonAsString)

  private def parseCaseClass[T](jsonAsString: String)(implicit m: Manifest[T]): Try[T] = {
    Try(replaceIdAndIsoDate(jsonAsString)) map { jsonAsStr =>
      parse(jsonAsStr)
    } flatMap {
      case o: JObject => Success(o.asInstanceOf[JObject])
      case no => Failure(new ParseException(s"can't parse as json object: $jsonAsString", null))
    } map {
      _ transformField {
        case (k, v) => (k.toLowerCase, v)
      }
    } flatMap { a =>
      implicit val formats = DefaultFormats
      Try(a.extract[T])
    }
  }

  private def replaceIdAndIsoDate(jsonAsString: String): String = {
    val s1 = replaceIdRegex1.replaceAllIn(jsonAsString, """{"mongoId": "$1",""")
    val s2 = replaceIdRegex2.replaceAllIn(s1, """{"mongoId": "$1",""")
    replaceIsoDateRegex.replaceAllIn(s2, "\"$1\"")
  }

  def promoAsJsonString(promo: Promotion): String = {
    implicit val formats = DefaultFormats
    val str = Serialization.write(promo)
    val (oneTwo, three) = str.splitAt(str.indexOf("cempresa"))
    val (one, two) = oneTwo.splitAt(oneTwo.indexOf("mongoid") + 7)
    one.toUpperCase + two + three.toUpperCase
  }

  def promoAsFixedFormat(promo: Promotion): String = {
    Seq(promo.finiefec, promo.ffinefec, promo.choraini, promo.chorafin, promo.cooferta, promo.cpromoci)
      .mkString(" ").trim
  }

  def refPropsAsFixedFormat(ref: Reference): String = {
    Seq(ref.cdivisio, ref.cfabrica, ref.cmarmuma).mkString(" ").trim
  }
}



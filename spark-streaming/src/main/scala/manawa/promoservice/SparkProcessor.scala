package manawa.promoservice

import manawa.promoservice.dao.{MongoDao, RedisDao}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.util.{Failure, Success, Try}

trait SparkProcessor extends Serializable {
  dependencies: KafkaStreamProvider with MongoDao with RedisDao with PromoRefParser =>

  def startApp(kafkaBrokers: String): Unit = {
    val ssc: StreamingContext = {
      val sparkConf = new SparkConf().setAppName("PromotionsCorporativeService")
      new StreamingContext(sparkConf, Seconds(2))
    }

    // ------------- Promos -------------
    val kafkaPromoMessages = createMessagesStream(ssc, kafkaBrokers, "promos").repartition(4)
    val promos = kafkaPromoMessages.map(_._2)
    val parsedPromos = promos map parsePromotion
    val promoResults = parsedPromos map processPromoAndPrint

    val promoResult = promoResults reduce { case (res1, res2) =>
      (res1._1 + res2._1, res1._2 + res2._2, res1._3 + res2._3)
    } map { case (promosNum, alreadyDoneNum, errorsNum) =>
      s" ==========  Promos: $promosNum, doneBefore: $alreadyDoneNum, Errors: $errorsNum =========="
    }
    promoResult.print()

    // ------------- Refs -------------
    val kafkaRefsMessages = createMessagesStream(ssc, kafkaBrokers, "refs").repartition(4)
    val refs = kafkaRefsMessages.map(_._2)
    val parsedRefs = refs map parseRef

    val combinedRefs = parsedRefs map (_.map(p => Seq(p))) reduce { case (t1, t2) =>
      t1.flatMap(s1 => t2.map(_ ++ s1))
    }

    val saveRefIdsResult: DStream[Try[Seq[String]]] = combinedRefs map (_ flatMap {refs =>
      refs.grouped(100)
        .map(saveRefsToRedis)
        .reduce[Try[Seq[String]]] {case (t1, t2) => t1.flatMap(s1 => t2 map (_ ++ s1))}
    })

    val updStatusNumsAndAllRefsNums = saveRefIdsResult map { refIdsTry =>
      refIdsTry flatMap { refsIds =>
        val savedNumTry: Try[Int] = refsIds.grouped(100).map[Try[Int]](updateStatusMulti(refsDb, _, "done"))
          .foldLeft[Try[Int]](Success(0)) { case (t1, t2) =>
          t1.flatMap[Int](a1 => t2.map(_ + a1))
        }

        savedNumTry map ((_, refsIds.size))
      }
    }

    val refResult = updStatusNumsAndAllRefsNums map {
      case Success((updatedRefStatusesNum, refsNum)) =>
        if (updatedRefStatusesNum == refsNum) {
          s" ==========  Refs: $refsNum =========="
        } else {
          s" ==========  Refs: $refsNum (updated $updatedRefStatusesNum statuses to mongo) =========="
        }
      case Failure(e) =>
        s" ========== Refs Error: ${e.getMessage} =========="
    }

    refResult.print()

    // ------------- Start Spark -------------
    ssc.start()
    ssc.awaitTermination()
  }

  private def processPromoAndPrint(promoTry: Try[Promotion]): (Int, Int, Int) = {
    promoTry match {
      case Success(promo) =>
        processPromo(promo) match {
          case Success(false) =>
            //            println(s" ------- Promo ${promo.mongoid} was already processed")
            (0, 1, 0)
          case Success(true) =>
            //            println(s" ------- Processed promo ${promo.mongoid}")
            (1, 0, 0)
          case Failure(e) =>
            //            println(s" ------- Error processing promo ${promo.mongoid}:\n ------- Error: ${e.getMessage}")
            (0, 0, 1)
        }
      case Failure(e) =>
        //        println(" ------- Error parsing promo: " + e.getMessage)
        (0, 0, 1)
    }
  }

  private def processPromo(promo: Promotion): Try[Boolean] = {
    isAlreadyDone(promo.mongoid) flatMap (if (_) Success(false) else savePromoAndCountRefs(promo))
  }

  private def savePromoAndCountRefs(promo: Promotion): Try[Boolean] = {
    val result = savePromoToRedis(promo)
    savePromoProcessResultToMongo(promo, result)
  }

  private def savePromoProcessResultToMongo(promo: Promotion, isDoneTry: Try[Boolean]): Try[Boolean] = {
    isDoneTry match {
      case Success(isDone) =>
        updatePromoStatus(promo.mongoid, "done") match {
          case Success(true) =>
            isDoneTry
          case Success(false) =>
            Failure(new PromoStatusUpdateException(promo, s"promo is saved",
              new RuntimeException("zero entries updated")))
          case Failure(e) =>
            Failure(new PromoStatusUpdateException(promo, s"promo is saved", e))
        }
      case Failure(e) =>
        updatePromoStatus(promo.mongoid, "fail", Some(e.getMessage)) match {
          case Success(true) =>
            isDoneTry
          case Success(false) =>
            Failure(new PromoStatusUpdateException(promo, "fail: " + e.getMessage, new RuntimeException("zero entries updated")))
          case Failure(e1) =>
            Failure(new PromoStatusUpdateException(promo, "fail: " + e.getMessage, e1))
        }
    }
  }

  private def processRefAndPrint(refTry: Try[Reference]): (Seq[String], Int) = {
    refTry match {
      case Success(ref) =>
        saveRefToRedis(ref) match {
          case Success(savedRefsNum) =>
            //            println(s" ------- Processed ref ${ref.mongoid}")
            (Seq(ref.mongoid), 0)
          case Failure(e) =>
            //            println(s" ------- Error processing ref ${ref.mongoid}:\n ------- Error: ${e.getMessage}")
            e.printStackTrace()
            (Seq.empty, 1)
        }
      case Failure(e) =>
        //        println(" ------- Error parsing ref: " + e.getMessage)
        (Seq.empty, 1)
    }
  }
}

package manawa.promoservice

case class Reference(
                     mongoid: String,
                     cempresa: String,
                     cdepartm: String,
                     cfamilia: String,
                     cbarraaa: String,
                     ctallaec: String,
                     cdivisio: String,
                     cfabrica: String,
                     cmarmuma: String)

case class Promotion(
                     mongoid: String,
                     cempresa: String,
                     cdepartm: String,
                     cfamilia: String,
                     cbarraaa: String,
                     ctallaec: String,
                     cdivisio: String,
                     cfabrica: String,
                     cmarmuma: String,
                     finiefec: String,
                     ffinefec: String,
                     choraini: String,
                     chorafin: String,
                     cnivelin: String,
                     cooferta: String,
                     cpromoci: String)